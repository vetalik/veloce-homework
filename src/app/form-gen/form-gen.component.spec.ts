import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGenComponent } from './form-gen.component';
import { FormatLabelPipe } from '../format-label.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('FormGenComponent', () => {
  let component: FormGenComponent;
  let fixture: ComponentFixture<FormGenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormGenComponent, FormatLabelPipe],
      imports: [ReactiveFormsModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create texts', () => {
    component.formInputs = [
      ['user', 'name'],
      ['e', 'mail'],
    ];
    const expectedValues = ['User Name', 'E Mail'];
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('label'));

    elements.forEach((el, i) => {
      expect(el.nativeElement.innerText).toEqual(expectedValues[i]);
    });
  });
});
