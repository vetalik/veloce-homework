import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';

export type IGeneratedEntrie = Array<{ fieldName: string; value: string }>;

@Component({
  selector: 'app-form-gen',
  templateUrl: './form-gen.component.html',
  styleUrls: ['./form-gen.component.scss'],
})
export class FormGenComponent implements OnInit, OnChanges {
  @Input() formInputs: Array<Array<string>> = [];
  @Output() genForm: EventEmitter<IGeneratedEntrie> = new EventEmitter();
  public model: { [key: string]: string } = {};

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formInputs) {
      this.model = changes.formInputs.currentValue.reduce((acc, val) => {
        return { ...acc, [val.join('_')]: '' };
      }, {});
    }
  }

  modelChange() {
    const res = Object.entries(this.model).map(([key, value]) => ({
      fieldName: key,
      value,
    }));
    this.genForm.emit(res);
  }
}
