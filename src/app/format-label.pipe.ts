import { Pipe, PipeTransform } from '@angular/core';
import { capitalCase } from 'change-case';

@Pipe({
  name: 'formatLabel',
})
export class FormatLabelPipe implements PipeTransform {
  transform(value: Array<string>, args?: any): any {
    return capitalCase(value.join('_'));
  }
}
