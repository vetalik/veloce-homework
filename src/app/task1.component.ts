import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IGeneratedEntrie } from './form-gen/form-gen.component';

@Component({
  templateUrl: './task1.component.html',
})
export class Task1Component implements OnInit {
  public content: string;
  public formInputs: Array<string> = [];
  public generatedEntreies: IGeneratedEntrie = [];
  public endResults: Array<string> = [];

  constructor() {}

  ngOnInit(): void {
    this.content = `<div #placeholder_first_name></div>
      <div #placeholder_last_name></div>`;
  }

  parseTemplate() {
    // const htmlData = this.sanitizer.bypassSecurityTrustHtml(this.content);
    const domparser = new DOMParser();
    const newhtml = domparser.parseFromString(this.content, 'text/html');
    NodeList.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];
    // @ts-ignore
    this.formInputs = Array.from(newhtml.querySelectorAll('div'))
      .filter(el => el.attributes[0].name.includes('#placeholder'))
      .map(el => el.attributes[0].name.split('_').slice(1));
  }

  displayResult() {
    this.endResults = this.generatedEntreies.map(({ fieldName, value }) => {
      return `<div #placeholder_${fieldName}><ng-content>${value}</ng-content></div>`;
    });
  }

  onUpdate(entries) {
    this.generatedEntreies = entries;
  }
}
