import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  templateUrl: './task2.component.html',
})
export class Task2Component implements OnInit, OnDestroy {
  public subscriptions: Array<Subscription> = [];
  public entries = [];
  public content;
  constructor(public activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer) {}
  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        tap(() => {
          this.entries = window.history.state.content ? window.history.state.content : [];
        }),
      )
      .subscribe();
    this.loadComponent();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public loadComponent() {
    const content = `<div>${this.entries.join(' ')}</div>`;
    this.content = this.sanitizer.bypassSecurityTrustHtml(content);
  }
}
